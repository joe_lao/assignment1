/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JApplet;

/**
 *
 * @author joelaohk
 */
public class BarChartVertical implements Graph {

    private static final float SERIESPADDING = 20;
    
    private BarChartVertical() {}
    
    private static BarChartVertical instance = new BarChartVertical();
    
    public static BarChartVertical getInstance() {
        return instance;
    }
    
    @Override
    public void drawGraph(JApplet app, Graphics g, GraphParams params, ColorScheme colorScheme) {
        ArrayList<int[]> dataSeries = params.getDataSeries();
        Color[] colors = colorScheme.getColors(params.getDataCount());
        final float width = app.getWidth()*0.85f;
        final float height = app.getHeight()*0.85f;
        final float barHeightRatio = height/(params.getLargestData()+100.0f);
        final float graphLeftPadding = app.getWidth()*0.035f;
        final float graphTopPadding = app.getHeight()*0.05f;
        final float seriesWidth = (width-40-(dataSeries.size()+1)*SERIESPADDING)/dataSeries.size();
        if (params.isShowChartTitle()) {
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
            g.drawString(params.getChartTitle(), app.getWidth()/2 - g.getFontMetrics().stringWidth(params.getChartTitle())/2, 60);
        }
        float currXPos = SERIESPADDING;
        for (int i = 0; i < dataSeries.size(); i++) {
            int[] dataSer = dataSeries.get(i);
            float barWidth = seriesWidth/dataSer.length;
            for (int j = 0; j < dataSer.length; j++) {
                g.setColor(colors[j]);
                g.fillRect((int)(currXPos+graphLeftPadding), (int) (height - (dataSer[j]*barHeightRatio) + graphTopPadding), (int)barWidth, (int)(dataSer[j]*barHeightRatio));
                if (params.isShowDataLabels()){
                    g.setColor(Color.black);
                    g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
                    float fontXPos = currXPos+graphLeftPadding+barWidth/2f-g.getFontMetrics().stringWidth(Integer.toString(dataSer[j]))/2f;
                    g.drawString(Integer.toString(dataSer[j]), (int)fontXPos, (int)(height-(dataSer[j]*barHeightRatio)+graphTopPadding) - 10);
                }
                    
                currXPos += barWidth;
            }
            currXPos += SERIESPADDING;
        }
        
        if (params.isShowAxisTitles()) {
            float xPos = SERIESPADDING + seriesWidth/2;
            for (int i = 0; i < params.getAxisValueV().length; i++) {
                g.setColor(Color.black);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.drawString(params.getAxisValueV()[i], 
                        (int) (xPos - g.getFontMetrics().stringWidth(params.getAxisValueV()[i])/2f + graphLeftPadding), 
                        (int) (app.getHeight()*0.95f));
                xPos += SERIESPADDING + seriesWidth;
            }
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
            g.drawString(params.getAxisTitleV(), 
                    (int)(width/2 + graphLeftPadding) - g.getFontMetrics().stringWidth(params.getAxisTitleV())/2, 
                    (int) (app.getHeight()*0.98f));
            g.drawString(params.getAxisTitleH(), (int) (app.getWidth()*0.02f), (int) (height/2 + 15f/2f));
        }
        
        
        if (params.isShowLegend()) {
            for (int i = 0; i < params.getAxisValueH().length; i++) {
                g.setColor(colors[i]);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.fillRect((int) (app.getWidth()*0.87f), (app.getHeight()/2)+30*i, 15, 15);
                g.drawString(params.getAxisValueH()[i], (int)(app.getWidth()*0.87f) + 30, (app.getHeight()/2)+30*i + 15);
            }
        }
    }
    
}
