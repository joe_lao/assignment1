/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JApplet;

/**
 *
 * @author joelaohk
 */
public class LineGraph implements Graph {

    private static LineGraph instance = new LineGraph();
    
    private LineGraph() {}
    
    public static LineGraph getInstance() {
        return instance;
    }
    
    @Override
    public void drawGraph(JApplet app, Graphics g, GraphParams params, ColorScheme colorScheme) {
        ArrayList<int[]> dataSeries = params.getDataSeries();
        Color[] colors = colorScheme.getColors(params.getDataCount());
        final float width = app.getWidth()*0.75f;
        final float height = app.getHeight()*0.9f;
        final float heightRatio = height/(params.getLargestData()+100.0f);
        final float graphLeftPadding = app.getWidth()*0.075f;
        final float graphTopPadding = app.getHeight()*0.05f;
        if (params.isShowChartTitle()) {
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
            g.drawString(params.getChartTitle(), app.getWidth()/2 - g.getFontMetrics().stringWidth(params.getChartTitle())/2, 60);
        }
        ((Graphics2D)g).setStroke(new BasicStroke(4));
        for (int i = 1; i < dataSeries.size(); i++) {
            
            for (int j = 0; j < params.getDataCount(); j++) {
                g.setColor(colors[j]);
                
                int point1X = (int) (((width/(dataSeries.size()-1))*(i-1))+graphLeftPadding);
                int point1Y = (int) (height - dataSeries.get(i-1)[j]*heightRatio+graphTopPadding);
                int point2X = (int) (((width/(dataSeries.size()-1))*(i))+graphLeftPadding);
                int point2Y = (int) (height - dataSeries.get(i)[j]*heightRatio+graphTopPadding);
                if (params.isShowDataLabels()){
                    g.setColor(Color.black);
                    g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
                    g.drawString(Integer.toString(dataSeries.get(i-1)[j]), point1X+5, point1Y);
                    g.drawString(Integer.toString(dataSeries.get(i)[j]), point2X+5, point2Y);
                    g.setColor(colors[j]);
                }
                System.out.println(String.format("[%d, %d, %d, %d]", point1X, point1Y, point2X, point2Y));
                g.drawLine(point1X, point1Y, point2X, point2Y);
            }
        }
        
        if (params.isShowAxisTitles()) {
            for (int i = 0; i < params.getAxisValueV().length; i++) {
                g.setColor(Color.black);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.drawString(params.getAxisValueV()[i], 
                        (int) ((width/(params.getAxisValueV().length-1))*i + graphLeftPadding) - g.getFontMetrics().stringWidth(params.getAxisValueV()[i])/2, 
                        (int) (app.getHeight()*0.95f));
            }
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
            g.drawString(params.getAxisTitleV(), 
                    (int)(width/2 + graphLeftPadding) - g.getFontMetrics().stringWidth(params.getAxisTitleV())/2, 
                    (int) (app.getHeight()*0.98f));
            g.drawString(params.getAxisTitleH(), (int) (app.getWidth()*0.02f), (int) (height/2 + 15f/2f));
        }
        
        
        if (params.isShowLegend()) {
            for (int i = 0; i < params.getAxisValueH().length; i++) {
                g.setColor(colors[i]);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.fillRect((int) (app.getWidth()*0.87f), (app.getHeight()/2)+30*i, 15, 15);
                g.drawString(params.getAxisValueH()[i], (int)(app.getWidth()*0.87f) + 30, (app.getHeight()/2)+30*i + 15);
            }
        }
        
    }
    
}
