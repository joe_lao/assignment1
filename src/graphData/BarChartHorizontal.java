/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JApplet;

/**
 *
 * @author joelaohk
 */
public class BarChartHorizontal implements Graph {
    
    private static final float SERIESPADDING = 20;
    
    private BarChartHorizontal() {}
    
    private static BarChartHorizontal instance = new BarChartHorizontal();
    
    public static BarChartHorizontal getInstance() {
        return instance;
    }

    @Override
    public void drawGraph(JApplet app, Graphics g, GraphParams params, ColorScheme colorScheme) {
        ArrayList<int[]> dataSeries = params.getDataSeries();
        Color[] colors = colorScheme.getColors(params.getDataCount());
        final float width = app.getWidth()*0.75f;
        final float height = app.getHeight()*0.95f;
        final float barHeightRatio = width/(params.getLargestData()+100.0f);
        final float graphLeftPadding = app.getWidth()*0.15f;
        final float graphTopPadding = app.getHeight()*0.05f;
        if (params.isShowChartTitle()) {
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
            g.drawString(params.getChartTitle(), app.getWidth()/2 - g.getFontMetrics().stringWidth(params.getChartTitle())/2, 60);
        }
        final float seriesWidth = (height-40-(dataSeries.size()+1)*SERIESPADDING)/dataSeries.size();
        float currYPos = SERIESPADDING + 30;
        for (int i = 0; i < dataSeries.size(); i++) {
            int[] dataSer = dataSeries.get(i);
            float barWidth = seriesWidth/dataSer.length;
            for (int j = 0; j < dataSer.length; j++) {
                g.setColor(colors[j]);
                g.fillRect((int)graphLeftPadding, (int)(currYPos+graphTopPadding), (int) (dataSer[j]*barHeightRatio), (int)barWidth);
                if (params.isShowDataLabels()){
                    g.setColor(Color.black);
                    g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
                    g.drawString(Integer.toString(dataSer[j]), (int) (dataSer[j]*barHeightRatio+graphLeftPadding) + 10, (int)(currYPos+graphTopPadding)+g.getFont().getSize());
                }
                    
                currYPos += barWidth;
            }
            currYPos += SERIESPADDING;
        }
        
        if (params.isShowAxisTitles()) {
            float yPos = SERIESPADDING + 30 + seriesWidth/2;
            for (int i = 0; i < params.getAxisValueV().length; i++) {
                g.setColor(Color.black);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.drawString(params.getAxisValueV()[i], 
                        (int) (app.getWidth()*0.07f), 
                        (int) (yPos + graphTopPadding));
                yPos += SERIESPADDING + seriesWidth;
            }
            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
            g.drawString(params.getAxisTitleV(), 
                    (int) (app.getWidth()*0.02f), 
                    (int) ((height/2) + graphTopPadding));
            g.drawString(params.getAxisTitleH(), (int) (width/2 + 15f/2f), (int) (app.getHeight()*0.99f));
        }
        
        
        if (params.isShowLegend()) {
            for (int i = 0; i < params.getAxisValueH().length; i++) {
                g.setColor(colors[i]);
                g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
                g.fillRect((int) (app.getWidth()*0.87f), (app.getHeight()/2)+30*i, 15, 15);
                g.drawString(params.getAxisValueH()[i], (int)(app.getWidth()*0.87f) + 30, (app.getHeight()/2)+30*i + 15);
            }
        }
    }
    
}
