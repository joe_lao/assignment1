/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

import java.util.ArrayList;
import javax.swing.JApplet;

/**
 *
 * @author joelaohk
 */
public class GraphParams {
    private String axisTitleH;
    private String axisTitleV;
    private String chartTitle;
    private int numOfDataSeries;
    private ArrayList<int[]> dataSeries;
    private String[] axisValueH;
    private String[] axisValueV;
    private boolean showChartTitle;
    private boolean showAxisTitles;
    private boolean showDataLabels;
    private boolean showLegend;
    
    private static GraphParams instance = null;
    private GraphParams () {}
    
    public void initParams(JApplet app) {
        this.axisTitleH = app.getParameter("AxisTitleH");
        this.axisTitleV = app.getParameter("AxisTitleV");
        this.chartTitle = app.getParameter("ChartTitle");
        this.numOfDataSeries = Integer.parseInt(app.getParameter("NumOfDataSeries"));
        this.dataSeries = new ArrayList<>();
        for (int i = 0; i < numOfDataSeries; i++) {
            String dataRaw = app.getParameter("Data" + (i+1));
            String[] stringDataSeries = dataRaw.split(",");
            int[] data = new int[stringDataSeries.length];
            for (int j = 0; j < stringDataSeries.length; j++)
                data[j] = Integer.parseInt(stringDataSeries[j]);
            dataSeries.add(data);
        }
        this.axisValueH = app.getParameter("AxisValueH").split(",");
        this.axisValueV = app.getParameter("AxisValueV").split(",");
        this.showChartTitle = binToBool(Integer.parseInt(app.getParameter("ShowChartTitle")));
        this.showAxisTitles = binToBool(Integer.parseInt(app.getParameter("ShowAxisTitles")));
        this.showDataLabels = binToBool(Integer.parseInt(app.getParameter("ShowDataLabels")));
        this.showLegend = binToBool(Integer.parseInt(app.getParameter("ShowLegend")));
        
        System.out.println("All things done!");
    }
    
    
    public static GraphParams getInstance() {
        if (instance == null) 
            return new GraphParams();
        else
            return instance;
    }
    
    private boolean binToBool(int bin) {
        return bin != 0;
    }
    
    public int getLargestData() {
        int max = 0;
        for (int[] dataSer: dataSeries) {
            for (int data: dataSer) {
                if (data > max) max = data;
            }
        }
        return max;
    }
    
    public int getDataCount() {
        return this.axisValueH.length;
    }

    /**
     * @return the axisTitleH
     */
    public String getAxisTitleH() {
        return axisTitleH;
    }

    /**
     * @param axisTitleH the axisTitleH to set
     */
    public void setAxisTitleH(String axisTitleH) {
        this.axisTitleH = axisTitleH;
    }

    /**
     * @return the axisTitleV
     */
    public String getAxisTitleV() {
        return axisTitleV;
    }

    /**
     * @param axisTitleV the axisTitleV to set
     */
    public void setAxisTitleV(String axisTitleV) {
        this.axisTitleV = axisTitleV;
    }

    /**
     * @return the chartTitle
     */
    public String getChartTitle() {
        return chartTitle;
    }

    /**
     * @param chartTitle the chartTitle to set
     */
    public void setChartTitle(String chartTitle) {
        this.chartTitle = chartTitle;
    }

    /**
     * @return the numOfDataSeries
     */
    public int getNumOfDataSeries() {
        return numOfDataSeries;
    }

    /**
     * @param numOfDataSeries the numOfDataSeries to set
     */
    public void setNumOfDataSeries(int numOfDataSeries) {
        this.numOfDataSeries = numOfDataSeries;
    }

    /**
     * @return the dataSeries
     */
    public ArrayList<int[]> getDataSeries() {
        return dataSeries;
    }

    /**
     * @param dataSeries the dataSeries to set
     */
    public void setDataSeries(ArrayList<int[]> dataSeries) {
        this.dataSeries = dataSeries;
    }

    /**
     * @return the axisValueH
     */
    public String[] getAxisValueH() {
        return axisValueH;
    }

    /**
     * @param axisValueH the axisValueH to set
     */
    public void setAxisValueH(String[] axisValueH) {
        this.axisValueH = axisValueH;
    }

    /**
     * @return the axisValueV
     */
    public String[] getAxisValueV() {
        return axisValueV;
    }

    /**
     * @param axisValueV the axisValueV to set
     */
    public void setAxisValueV(String[] axisValueV) {
        this.axisValueV = axisValueV;
    }

    /**
     * @return the showChartTitle
     */
    public boolean isShowChartTitle() {
        return showChartTitle;
    }

    /**
     * @param showChartTitle the showChartTitle to set
     */
    public void setShowChartTitle(boolean showChartTitle) {
        this.showChartTitle = showChartTitle;
    }

    /**
     * @return the showAxisTitles
     */
    public boolean isShowAxisTitles() {
        return showAxisTitles;
    }

    /**
     * @param showAxisTitles the showAxisTitles to set
     */
    public void setShowAxisTitles(boolean showAxisTitles) {
        this.showAxisTitles = showAxisTitles;
    }

    /**
     * @return the showDataLabels
     */
    public boolean isShowDataLabels() {
        return showDataLabels;
    }

    /**
     * @param showDataLabels the showDataLabels to set
     */
    public void setShowDataLabels(boolean showDataLabels) {
        this.showDataLabels = showDataLabels;
    }

    /**
     * @return the showLegend
     */
    public boolean isShowLegend() {
        return showLegend;
    }

    /**
     * @param showLegend the showLegend to set
     */
    public void setShowLegend(boolean showLegend) {
        this.showLegend = showLegend;
    }
}
