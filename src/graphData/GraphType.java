/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

/**
 *
 * @author joelaohk
 */
public enum GraphType {
    
    LINE("Line", LineGraph.getInstance()), 
    BARH("Bar (Horizontal)", BarChartHorizontal.getInstance()),
    BARV("Bar (Vertical)", BarChartVertical.getInstance());
    
    private final String displayName;
    private final Graph graphObject;
    private GraphType(String displayName, Graph graphObject) {
        this.displayName = displayName;
        this.graphObject = graphObject;
    }
    
    public String getName() {
        return displayName;
    }
    
    public Graph getGraphObject() {
        return graphObject;
    }
}
