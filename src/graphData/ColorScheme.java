/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphData;

import java.awt.Color;

/**
 *
 * @author joelaohk
 */
public enum ColorScheme {
    RAINBOW("Rainbow"),
    GRASS("Grass"),
    GRAYSCALE("Grayscale");
    
    private final String displayName;
    
    private ColorScheme(String displayName) {
        this.displayName = displayName;
    }
    
    public String getName() {
        return this.displayName;
    }
    
    public Color[] getColors(int n) {
        Color[] c = new Color[n];
        switch(this) {
            case RAINBOW:
                for (int i = 0; i < n; i++) {
                    Color cc = Color.getHSBColor(((359f/n)*i)/359f, 0.9f, 0.8f);
                    c[i] = cc;
                }
                break;
            case GRAYSCALE:
                for (int i = 0; i < n; i++) {
                    float grayLevel = (255.0f/n)*i;
                    Color cc = new Color((int)grayLevel, (int)grayLevel, (int)grayLevel);
                    c[i] = cc;
                }
                break;
            default:
                for (int i = 0; i < n; i++) {
                    Color cc = Color.getHSBColor (0.33f, 0.9f, ((79f/n)*i+20f)/100f);
                    c[i] = cc;
                }
                break;
        }
        return c;
    }
    
}
