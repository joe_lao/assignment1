package graphData;

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JApplet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joelaohk
 */
public interface Graph {
    
    public void drawGraph(JApplet app, Graphics g, GraphParams params, ColorScheme colorScheme);
}
