package graphData;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author chlao2
 */
public class Main extends JApplet implements ItemListener {

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    
    Choice chartType = new Choice();
    Choice colorScheme = new Choice();
    Checkbox showLabels = new Checkbox();
    ColorScheme currentColors;
    GraphType currentGraphType;
    GraphParams params;
    
    @Override
    public void init() {
        for (GraphType ct : GraphType.values()) {
            chartType.add(ct.getName());
        }
        chartType.addItemListener(this);
        
        for (ColorScheme sc: ColorScheme.values()) {
            colorScheme.add(sc.getName());
        }
        colorScheme.addItemListener(this);
        
        showLabels.setLabel("Show Labels");
        showLabels.setState(true);
        showLabels.addItemListener(this);
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(chartType);
        panel.add(colorScheme);
        panel.add(showLabels);
        setLayout(new BorderLayout());
        add(panel, BorderLayout.NORTH);
        
        params = GraphParams.getInstance();
        params.initParams(this);
        currentGraphType = GraphType.values()[0];
        currentColors = ColorScheme.values()[0];
    }

    // TODO overwrite start(), stop() and destroy() methods

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == chartType) {
            for (GraphType ct: GraphType.values()) {
                if (chartType.getSelectedItem().equals(ct.getName()))
                    currentGraphType = ct;
            }
        } else if (e.getSource() == colorScheme) {
            for (ColorScheme sc: ColorScheme.values()) {
                if (colorScheme.getSelectedItem().equals(sc.getName()))
                    currentColors = sc;
            }
        } else {
            params.setShowDataLabels(showLabels.getState());
        }
        
        repaint();
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        currentGraphType.getGraphObject().drawGraph(this, g2d, params, currentColors);
    }
}
